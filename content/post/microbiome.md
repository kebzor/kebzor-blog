---
title: "Všechny nemoci začínají ve střevech"
date: 2019-08-19T19:24:44+01:00
draft: false
categories:
  - "Diet"
tags:
  - "Microbiome"
  - "Leaky Gut"

---


## Zvýšená propustnost střev

Častým spouštěčem autoimunitních onemocnění je zvýšená propustnost střev. Takto poškozené střevo propouští do krevního oběhu látky co tam vůbec nepatří. Autoimunitní systém začne útočit na tyto nevítané hosty a často zaútočí i na vlastni tělové buňky.

### Projevy
Zvýšená propustnost střeva může vést k mnoha onemocněním jako jsou například:

- Chronická zácpa nebo průjem

- Slabý imunitní systém

- Bolesti hlavy, zapomínání

- Nadměrná únava

- Problémy s pokožkou, akné, ekzémy

- Chuť na sladké

- Revmatoidní artritida

- Celiakie

- Cukrovka

- Crohnova choroba

- IBS

- Roztroušená skleróza

- Alergie na potraviny



## Jak si pomoci
- Omezte rafinované cukry – tyto cukry jsou potravou pro škodlivé bakterie žijící ve střevě. Tyto bakterie nejen, že poškozují vaši střevní stěnu. ale také  mají vliv na vaši náladu.

- Jezte fermentované jídla, která naopak střevu dodávají prospěšné bakterie.

- Jezte spoustu vlákniny – vláknina je právě potravou pro prospěšné bakterie.

- Omezte Nesteroidní antiflogistika a antibiotika – léky jako aspirin, ibuprofen ván možná krátkodobě pomohou od bolesti, ale z dlouhodobého hlediska škodí střevní mikroflóře.

> Klíčem ke zdravému střevu je zdravá střevní mikroflóra.

