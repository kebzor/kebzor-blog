---
title: "Reset imunitního systému
"
date: 2019-08-19T19:24:44+01:00
draft: false
categories:
  - "Diet"
tags:
  - "Microbiome"
  - "Autophagy"
---

## Autophagie
Autofagie je proces při kterém se poškozené a nefunkční součástky buněk sbírají a rozkládají na stavební kameny, které jsou použity na tvorbu nových plně funkčních součástek.

Autofagie na vysoké úrovni může rozložit na součástky vadne bílé krvinky, které u autoimunitních poruch útočí na vlastní buňky.
### Půst delší 72 hodin může resetovat váš imunitní systém

Když hladovíme tělo se snaží ušetřit energii. Jeden ze způsobů, jak ušetřit energii je recyklovat spoustu imunitních buněk které nejsou třeba a obzvlášť ty které mohou být poškozené.
Je prokázáno, že počet bílých krvinek které jsou zodpovědné za náš imunitní systém se výrazně sníží při půstu delším než 72 hodin.
Po ukončení půstu se vytvoří úplně nové bílé krvinky a jejich počet se zvýší na počáteční hodnotu.

> Třídení půst může restartovat váš imunitní systém.

### Půst napodobující dieta

Při této dietě si tělo myslí, že se postíte a ziskate tak výhody půstu a přitom můžete jíst.

#### Makra

První den 1090 kalorií:

- 34% karbohydráty
- 10% proteiny
- 56% tuky

Druhý až pátý den 725 kalorií:

- 47% karbohydráty
- 9% proteiny
- 44% tuky

#### Nejjednodušší varianta

Pokud se nechcete počítat s kaloriemi, a přesto vyzkoušet tuto dietu stačí jist jen dvě avokáda denně po doby pěti dní. Budete překvapeni jak skvěle se budete cítit.
