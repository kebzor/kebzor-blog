---
title: "O mně"
date: 2019-08-19T20:06:45+01:00
menu: "main"
---


## Míra jmenuju, rady sděluju

### Jak to začalo

V roce 2014 jsem se dozvěděl, že mám roztroušenou sklerózu a byl jsem odeslán ke spacialistovy. Trvalo to několik měsíců než jsem se dostal na první schůzku a já měl dost času hledat informace stran nemoci a možné léčby. A položil jsem si následující otázku.

#### Je něco co pro své zdrví mužů udělat já sám?

Mnozí si tuhle otázku položili a změnou životního stylu dokázali průběh roztrusene sklerózy zvrátit. Prostě se začali uzdravovat.

Dvě jména prikaldem

1. Terry Wahls [Officialni web](https://terrywahls.com/)

2. George Jelínek [Oficiální web](https://overcomingms.org/)

*Tak jsem změnil od základu svůj jídelníček.*

#### Návštěva specialisty

Když jsem se konečně dostal ke specialistovy tak už jsem věděl, že žádnou běžnou léčbu na upravení/potlačení vlastní imunity nechci, a že si pořadím sám.

> Naše tělo má úžasnou shopnost regenerace stačí jen když mu nehazeme klacky pod nody v podobě západního životního stylu a západní diety

## Jak to pokračuje?

Moje diate je moje medicína .

Můj stav se pozvolna zlepšuje.
